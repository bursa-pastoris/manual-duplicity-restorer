**Note:** since 2023-03-15, this script is part of
<https://git.disroot.org/bursa-pastoris/scripts>.  Therefore, this repository
is archived and will no longer be updated.  `manual-duplicity-restorer.py` is
still maintained as part of the linked repository.


## How-to

This is a script to restore data from corrupted duplicity backups without the
`manifest` file, maintaining the original directory tree.

The script requires unencrypted archives.  If you encrypted them, you must
decrypt them massively with

    gpg --multifile --decrypt path/to/encrypted/archives/duplicity-full*.difftar.gpg

This will decrypt all the archives in the same path.  Depending on their size
it can require some time and may require you to type your passphrase multiple
times.

The decrypted archives must be extracted with `tar`:

    tar xvf path/to/decrypted/archives/*.difftar --directory=restore/path

After that, in `restore/path` there will be two path trees:
- `snapshot` contains files large at most 64KB
- `multivol-snapshot` contains all the larger files: each of them is split in
  fragments large up to 64KB each, gathered in a directory with the same name
of the original file

For example, an original file `Documents/bank-account-data.md` large 100B will
be available right after the extraction as a single file stored in
`restore/path/snapshot/Documents/`.
`Download/how-to-crack-assassins-creed.pdf` large 200KB will be split in
200/64=3.125 -> 4 files, stored in
`restore/path/multivol-snapshot/Downloads/how-to-crack-assassins-creed.pdf/`.

This script takes as input the *full* paths to `snapshot`, `multivol_snapshot`
and `restore/path` and restores all the files from the first two to the third.

The three input parameters must be written directly into the script editing the
values of `FROM_SNAPSHOT`, `FROM_MULTIVOL` and `TO` variables respectively.
`TO` path must *not* exist when the script is launched.

## License

The script is released under the [Expat license](./LICENSE) (also known as the
potentially misleading name of "MIT license").
